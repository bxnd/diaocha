<?php
namespace app\index\validate;

use think\Validate;

class Openanswer extends Validate
{
    protected $rule = [
        'member'  =>  'require|max:100',
        'nickname'  =>  'require|max:100',
        // 'answer'  =>  'require',
        // 'nickname'  =>  'require|max:100|unique:guest',
    ];

    protected $message  =   [
        'member.require' => '姓名不能为空！',  
        'member.unique' => '您已答题！', 
        'nickname.require' => '学号不能为空！', 
        'nickname.unique' => '对不起，你不在问卷范围，谢谢参加！',  
        'member.max' => '姓名不能大于30位！', 
        'keywords.require' => '关键字不能为空！', 

    ];



}