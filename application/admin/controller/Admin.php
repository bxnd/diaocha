<?php

namespace app\admin\controller;

use think\Env;

class Admin extends Base
{
    public function qrcode($url, $size = 4)
    {
        Vendor('Phpqrcode.phpqrcode');
        // 如果没有http 则添加
        if (strpos($url, 'http') === false) {
            $url = 'http://'.$url;
        }
        QRcode::png($url, false, QR_ECLEVEL_L, $size, 2, false, 0xFFFFFF, 0x000000);
    }

    public function lst()
    {
        // $Test = new \my\Test();
        // $Test->sayHello();

        // $Test = new \Phpqrcode\Phpqrcode();
        // $Test->PHPExcel();


        $adminres = \think\Db::name('admin')->paginate(10);
        $this->assign('adminres', $adminres);
        return $this->fetch();
    }

    public function add()
    {
        if (request()->isPost()) {
            $data     = [
                'username' => input('username'),
                'password' => md5(input('password')),
            ];
            $validate = \think\Loader::validate('Admin');
            if ($validate->check($data)) {
                $db = \think\Db::name('admin')->insert($data);
                if ($db) {
                    return $this->success('添加管理员成功！', 'lst');
                } else {
                    return $this->error('添加管理员失败！');
                }
            } else {
                return $this->error($validate->getError());
            }
            return;
        }
        return $this->fetch();
    }

    public function edit()
    {
        $id = input('id');
        if (request()->isPost()) {
            if (Env::get('demo.debug')) {
                return $this->success('演示模式， 管理员密码禁止修改！');
            }
            $userinfo = \think\Db::name('admin')->find($id);
            $password = $userinfo['password'];
            $data     = [
                'id'       => input('id'),
                'username' => input('username'),
                'password' => input('password') ? md5(input('password')) : $password,
            ];
            $validate = \think\Loader::validate('Admin');
            if ($validate->scene('edit')->check($data)) {
                if ($db = \think\Db::name('admin')->update($data)) {
                    return $this->success('修改管理员成功！', 'lst');
                } else {
                    return $this->error('修改管理员失败！');
                }
            } else {
                return $this->error($validate->getError());
            }


            return;
        }
        $admins = db('admin')->find($id);
        $this->assign('admins', $admins);
        return $this->fetch();
    }

    public function del()
    {
        $id = input('id');
        if ($id == 1) {
            return $this->error('初始化管理员不允许删除！');
        }
        if (db('admin')->delete($id)) {
            return $this->success('删除管理员成功！', 'lst');
        } else {
            return $this->error('删除管理员失败！');
        }
    }


}
