<?php
namespace app\admin\controller;
use think\Controller;
class Article extends Base
{
    public function lst()
    {
        if (session('username') == 'admin') {
            # code...
            $artres= \think\Db::name('article')->alias('a')->join('cate c','c.id = a.cateid','LEFT')->order('a.id desc')->field('a.id,a.title,a.pic,a.click,a.close,a.time,c.catename')->paginate(10);
        }else{
            //return 2;
            $artres= \think\Db::name('article')->alias('a')->join('cate c','c.id = a.cateid','LEFT')->where('a.username','=',session('username'))->order('a.id desc')->field('a.id,a.title,a.close,a.pic,a.click,a.time,c.catename')->paginate(10);
        }

        $this->assign('artres',$artres);
        return $this->fetch();
    }

    public function add()
    {
        if(request()->isPost()){
            $data=[
            'title'=>input('title'),
            'keywords'=>input('keywords'),
            'desc'=>input('desc'),
            'cateid'=>input('cateid'),
            'content'=>input('content'),
            'time'=>time(),
            ];
            if($_FILES['pic']['tmp_name']){
                // 获取表单上传文件 例如上传了001.jpg
                $file = request()->file('pic');
                // 移动到框架应用根目录/public/uploads/ 目录下
                // echo ROOT_PATH ;  die;
                $info = $file->move(ROOT_PATH . 'public' . DS . '/static/uploads');
                // var_dump($info); die;
                if($info){
                    // 成功上传后 获取上传信息
                    $data['pic']='/static/uploads/'.date('Ymd').'/'.$info->getFilename();
                    // 输出 42a79759f284b767dfcb2a0197904287.jpg
                    // echo $info->getFilename();  die;
                }else{
                    // 上传失败获取错误信息
                 return $this->error($file->getError());
             }
         }
         $validate = \think\Loader::validate('article');
         if($validate->check($data)){
            $db= \think\Db::name('article')->insert($data);
            if($db){
                return $this->success('添加文章成功！','lst');
            }else{
                return $this->error('添加文章失败！');
            }
        }else{
            return $this->error($validate->getError());
        }

        return;
    }
    $cateres=db('cate')->select();
    $this->assign('cateres',$cateres);
    return $this->fetch();
}

public function edit()
{
    if(request()->isPost()){
        $data=[
        'id'=>input('id'),
        'title'=>input('title'),
        'keywords'=>input('keywords'),
        'desc'=>input('desc'),
        'cateid'=>input('cateid'),
        'content'=>input('content'),
        ];
        if($_FILES['pic']['tmp_name']){
                // 获取表单上传文件 例如上传了001.jpg
            $file = request()->file('pic');
                // 移动到框架应用根目录/public/uploads/ 目录下
                // echo ROOT_PATH ;  die;
            $info = $file->move(ROOT_PATH . 'public' . DS . '/static/uploads');
                // var_dump($info); die;
            if($info){
                    // 成功上传后 获取上传信息
                $data['pic']='/static/uploads/'.date('Ymd').'/'.$info->getFilename();
                    // 输出 42a79759f284b767dfcb2a0197904287.jpg
                    // echo $info->getFilename();  die;
            }else{
                    // 上传失败获取错误信息
             return $this->error($file->getError());
         }
     }
     $validate = \think\Loader::validate('article');
     if($validate->check($data)){
        $db= \think\Db::name('article')->update($data);
        if($db){
            return $this->success('修改文章成功！','lst');
        }else{
            return $this->error('修改文章失败！');
        }
    }else{
        return $this->error($validate->getError());
    }

    return;
}
$arts= \think\Db::name('article')->find(input('id'));
$cateres=db('cate')->select();
$this->assign('cateres',$cateres);
$this->assign('arts',$arts);
return $this->fetch();
}
public function statistics()
{

    $arts= \think\Db::name('article')->find(input('id'));

    $answercount=db('answer')->where('cate','=',input('id'))
    ->distinct(true)
    ->field('member')
    ->select();

    $answersum=db('answer')->where('cate','=',input('id'))
    ->field('cate,SUM(answer1),SUM(answer4),answer1')
    ->select();
        // var_dump($answersum);exit();

    $this->assign('arts',$arts);
        //$this->assign('answer',$answer);
    $this->assign('answercount',count($answercount));
    $this->assign('answersum',$answersum);


    return $this->fetch();
}

public function del(){
    if(db('article')->delete(input('id'))){
        return $this->success('删除问卷成功！','lst');
    }else{
        return $this->error('删除问卷失败！');
    }
}
public function close(){

    if (input('close')==1 ) {
        # code...
     $data=[
     'id'=>input('id'),
        // 'title'=>input('title'),
        // 'keywords'=>input('keywords'),
        // 'desc'=>input('desc'),
        // 'cateid'=>input('cateid'),
     'close'=>1,
     ];
     $db= \think\Db::name('article')->update($data);
     if($db){
        return $this->success('问卷已关闭！','lst');
    }else{
        return $this->error('关闭失败！');
    }
} else {
        # code...

    $data=[
    'id'=>input('id'),
        // 'title'=>input('title'),
        // 'keywords'=>input('keywords'),
        // 'desc'=>input('desc'),
        // 'cateid'=>input('cateid'),
    'close'=>0,
    ];
    $db= \think\Db::name('article')->update($data);
    if($db){
        return $this->success('问卷已打开！','lst');
    }else{
        return $this->error('打开失败！');
    }

}


}
public function code(){


    $value="http://www.weste.net"; 
    $errorCorrectionLevel = "L"; 
    $matrixPointSize = "4"; 
    QRcode::png($value, false, $errorCorrectionLevel, $matrixPointSize); 
    exit;  
}
public function born()
{
        //return input('id');
        //exit();
    $arts= \think\Db::name('cate')->find(input('id'));
    $cate_contens= \think\Db::name('cate_contens')->order('id asc')->where('cate', input('id'))->select();
        //$cateres=db('cate')->select();
        //$this->assign('cateres',$cateres);
        //return $arts['title'];
    $data=[

                //'gmt'=>input('gmt'),
    'title'=>$arts['catename'],
    'desc'=>$arts['title'],
    'cateid'=>$arts['id'],
    'time'=>time(), 
    'username'=>session('username'),
    ];
        //var_dump(input('gmt'));

    
        $db= \think\Db::name('article')->insertGetId($data);
        foreach($cate_contens as $user){

           $data2=[

           'title'=>$user['title'],
           'cate'=>$db,
           'type'=>$user['type'],
           'answer1'=>$user['answer1'],
           'answer2'=>$user['answer2'],
           'answer3'=>$user['answer3'],
           'answer4'=>$user['answer4'],
           'answer5'=>$user['answer5'],
           'answer6'=>$user['answer6'],

           ];
           $df= \think\Db::name('article_contens')->insert($data2);
       }  


       if($db){
        return $this->success('生成问卷成功！','lst');
    }else{
        return $this->error('生成问卷失败！');
    }

$this->assign('arts',$arts);
$this->assign('cateid',input('id'));
        //var_dump($cate_contens);
        //var_dump($arts);
        //exit();                      
return $this->fetch();
}
public function out(){
    $fileName = session('username').date('_YmdHis');
    header("Content-type:application/vnd.ms-excel;charset=utf-8");
    header("Content-Disposition:attachment;filename=$fileName.xls");


    $tab="\t"; $br="\n";
    $head="编号".$tab."问卷标题".$tab."问卷标题";
        // //输出内容如下： 
    echo $head.$br;

    if (session('username') == 'admin') {
            # code...
        $cateres= \think\Db::name('article')->order('id desc')->paginate(10000);
    }else{
        $cateres= \think\Db::name('article')->where('username','=',session('username'))->order('id desc')->paginate(10000);
    }
        //var_dump($cateres);
        // 获取数据集记录数
    $count = count($cateres);
        // 遍历数据集
    foreach($cateres as $user){
        echo $user['id'].$tab;
        echo $user['title'].$tab; 
        echo $user['desc'];
        echo $br;
    }



}











}
