<?php

namespace app\admin\controller;

use think\Controller;

class Member extends Base
{
    public function lst()
    {
        //return "13";
        //exit;
        // $artres= \think\Db::name('member')->alias('a')->join('cate c','c.id = a.cateid','LEFT')->order('a.id desc')->field('a.id,a.title,a.pic,a.click,a.time,c.catename')->paginate(4);
        // $this->assign('artres',$artres);

        if (session('username') == 'admin') {
            $artres = \think\Db::name('member')->order('id desc')->paginate(10);

        } else {
            $artres = \think\Db::name('member')->where('username', '=', session('username'))->order('id desc')->paginate(10);
        }
        $articleid = input('articleid');

        $this->assign('articleid', $articleid);
        $this->assign('artres', $artres);

        //var_dump($artres);
        return $this->fetch();
    }

    public function answerlst_view()
    {

        $name = input('name');

        $artres = \think\Db::name('answer')
            ->where('name', '=', $name)
            ->order('id asc')->select();


        $articleid = input('articleid');

        $this->assign('articleid', $articleid);
        $this->assign('artres', $artres);

        // dump($artres);

        foreach ($artres as $key => $user) {
            echo '   [a]   ' . $user['answer1'] . "";
            echo '   [b]   ' . $user['answer2'] . "";
            echo '   [c]   ' . $user['answer3'] . "";
            echo '   [d]   ' . $user['answer4'] . "";
            echo '   [e]   ' . $user['answer5'] . "";
            echo '   [f]    ' . $user['answer6'] . "<br />";
            // echo $user->answer2 . "<br />";
        }
        // return $this->fetch();
    }

    public function answerlst_details()
    {

        $name = input('member');

        $artres = \think\Db::name('member')
            ->where('name', '=', $name)
            ->order('id desc')
            ->paginate(10, false,
                [
                    'type'     => 'Bootstrap',
                    'var_page' => 'page',
                    'query'    => ['member'       => input('member'),
                                   'serialnumber' => input('serialnumber')
                    ],
                ]

            );

        $articleid = input('articleid');

        $this->assign('articleid', $articleid);
        $this->assign('artres', $artres);

        //var_dump($artres);
        return $this->fetch();
    }

    public function answerlst()
    {


        $artres = \think\Db::name('answer')
            ->alias('a')
            ->join('member c', 'c.id = a.memberid', 'LEFT')
            ->where('a.cate', '=', input('cate'))
            ->where('a.answerid', '=', input('answerid'))
            ->field('a.id,a.member,a.name,a.cate,a.serialnumber,c.school,c.area,c.city')
            ->order('a.id desc')
            ->paginate(200, false,
                [
                    'type'     => 'Bootstrap',
                    'var_page' => 'page',
                    'query'    => ['cate'     => input('cate'),
                                   'answerid' => input('answerid')
                    ],
                ]

            );


        // var_dump($artres);exit();

        // $artres= \think\Db::name('article')
        // ->alias('a')
        // ->join('cate c','c.id = a.cateid','LEFT')
        // ->order('a.id desc')
        // ->field('a.id,a.title,a.pic,a.click,a.close,a.time,c.catename')
        // ->paginate(10);


        $articleid = input('articleid');

        $this->assign('articleid', $articleid);
        $this->assign('artres', $artres);

        //var_dump($artres);
        return $this->fetch();
    }

    public function add()
    {


        if (request()->isPost()) {


            // if (trim(input('serialnumber'))) {
            //     # code...
            //     echo "99";
            // } else {
            //     # code...
            //     echo "88";
            // }


            // var_dump(input('serialnumber'));exit();

            $data = [
                'name'         => input('name'),
                'telephone'    => trim(input('telephone')) ? input('telephone') : '0',
                'serialnumber' => trim(input('serialnumber')) ? input('serialnumber') : '0',
                'province'     => input('province'),
                'area'         => input('area'),
                'school'       => input('school'),
                'grade'        => input('grade'),
                'class'        => input('class'),
                'type'         => input('type'),
                'level'        => input('level'),
                'city'         => input('city'),
                'username'     => session('username'),
                'number'       => trim(input('number')) ? input('number') : "0",
                'time'         => time(),
            ];

            $validate = \think\Loader::validate('Member');

            if ($validate->check($data)) {
                //exit;
                $db = \think\Db::name('member')->insert($data);
                if ($db) {
                    return $this->success('添加用户成功！', 'lst');
                } else {
                    return $this->error('添加用户失败！');
                }
            } else {
                return $this->error($validate->getError());
            }

            return;
        }
        $cateres = db('cate')->select();
        $this->assign('cateres', $cateres);
        return $this->fetch();
    }

    public function limit()
    {
        if (request()->isPost()) {
            $data = [
                'limit'     => input('limit'),
                'articleid' => input('articleid'),
                'type'      => input('type'),
                'time'      => time(),
            ];


            $db = \think\Db::name('article_limit')->where('articleid', input('articleid'))->update($data);
            if ($db) {
                return $this->success('修改指定用户成功！', 'article/lst');
            } else {
                //return $this->error('修改用户信息失败！');
                $db = \think\Db::name('article_limit')->insert($data);
                return $this->success('设置指定用户成功！', 'article/lst');
            }

        }
        $arts    = \think\Db::name('artres')->find(input('id'));
        $cateres = db('cate')->select();
        $this->assign('cateres', $cateres);
        $this->assign('arts', $arts);
        return $this->fetch();
    }

    public function edit()
    {
        if (request()->isPost()) {
            $data = [
                'name'         => input('name'),
                'telephone'    => input('telephone'),
                'province'     => input('province'),
                'area'         => input('area'),
                'school'       => input('school'),
                'grade'        => input('grade'),
                'class'        => input('class'),
                'serialnumber' => input('serialnumber'),
                'type'         => input('type'),
                'level'        => input('level'),
                'city'         => input('city'),
                'username'     => input('username'),
                'number'       => input('number'),
                'time'         => time(),
            ];

            $validate = \think\Loader::validate('member');
            if ($validate->check($data)) {
                $db = \think\Db::name('member')->where('id', input('id'))->update($data);
                if ($db) {
                    return $this->success('修改用户信息成功！', 'lst');
                } else {
                    return $this->error('修改用户信息失败！');
                }
            } else {
                return $this->error($validate->getError());
            }

            return;
        }
        $arts    = \think\Db::name('member')->find(input('id'));
        $cateres = db('cate')->select();
        $this->assign('cateres', $cateres);
        $this->assign('arts', $arts);
        return $this->fetch();
    }

    public function del()
    {

        if (input('alldel')) {
            # code...
            //echo input('alldel');
            // $str = "/20140906/201409060718317800.jpg#####/20140906/201409060718124874.jpg#####/20140906/201409060718124874.jpg#####/20140906/201409060718124874.jpg";
            $str = input('alldel');
            $arr = explode(",", $str);
            foreach ($arr as $a) {
                //echo $a."</br>";
                if (db('member')->delete($a)) {
                    //return $this->success('删除用户成功！','lst');
                    //echo "删除了用户：".$a."</br>";
                } else {
                    //return $this->error('删除用户失败！');
                }
            }
            return $this->success('批量删除用户成功！', 'lst');

        } else {
            # code...
            if (db('member')->delete(input('id'))) {
                return $this->success('删除用户成功！', 'lst');
            } else {
                return $this->error('删除用户失败！');
            }

        }


    }

    public function upload()
    {

        if (request()->file('image')) {

            $file = request()->file('image');
            $info = $file->move(ROOT_PATH . 'vendor' . DS . '/PHPExcel66/Documentation/Examples/Reader/sampleData');
            if ($info) {

                $datapic = '/' . date('Ymd') . '/' . $info->getFilename();
                $url     = '/gmt/vendor/PHPExcel66/Documentation/Examples/Reader/exampleReader01.php?name=' . $datapic . '&m=1&username=' . session('username');
                echo '<h2><a href="' . $url . '"> 上传成功，提交导入用户</a></h2>';
            } else {

                echo $file->getError();
            }
        } else {
            return "没有选择文件";
        }
    }

    public function join()
    {

        return $this->fetch();
    }


    public function out()
    {
        $fileName = session('username') . date('_YmdHis');
        header("Content-type:application/vnd.ms-excel;charset=utf-8");
        header("Content-Disposition:attachment;filename=$fileName.xls");


        $tab  = "\t";
        $br   = "\n";
        $head = "ID" . $tab . "姓名" . $tab . "省" . $tab . "市" . $tab . "县/区" . $tab . "学校" . $tab . "年级" . $tab . "班级" . $tab . "学号" . $tab . "电话" . $tab . "类型" . $tab . "级别" . $tab . "编号" . $tab;
        // //输出内容如下： 
        echo $head . $br;

        if (session('username') == 'admin') {
            # code...
            $cateres = \think\Db::name('member')->order('id desc')->paginate(10000);
        } else {
            $cateres = \think\Db::name('member')->where('username', '=', session('username'))->order('id desc')->paginate(10000);
        }
        //var_dump($cateres);
        // 获取数据集记录数
        $count = count($cateres);
        // 遍历数据集
        foreach ($cateres as $user) {
            echo $user['id'] . $tab;
            echo $user['name'] . $tab;
            echo $user['province'] . $tab;
            echo $user['city'] . $tab;
            echo $user['area'] . $tab;
            echo $user['school'] . $tab;
            echo $user['grade'] . $tab;
            echo $user['class'] . $tab;
            echo $user['serialnumber'] . $tab;
            echo $user['telephone'] . $tab;
            echo $user['type'] . $tab;
            echo $user['level'] . $tab;
            echo $user['number'] . $tab;
            echo $br;
        }


    }


}
