<?php
namespace app\admin\validate;

use think\Validate;

class Cate extends Validate
{
    protected $rule = [
        // 'catename'  =>  'require|max:25|unique:cate',
        'catename'  =>  'require',
        // 'keywords'  =>  'require',
        // 'title1'  =>  'require',
    ];

    protected $message  =   [
        'catename.require' => '问卷名称不能为空！', 
        'title1.require' => '问题不能为空！', 
        'catenamey.require' => '栏目名称不能为空！',  
        'catenamey.unique' => '栏目名称不能重复！',  
        'catenamey.max' => '栏目名称不能大于25位！', 
        'keywordsy.require' => '栏目关键字不能为空！', 

    ];

    protected $scene = [
        'edit'  =>  ['catename'],
    ];


}