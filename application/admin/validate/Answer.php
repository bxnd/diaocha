<?php
namespace app\admin\validate;

use think\Validate;

class Article extends Validate
{
    protected $rule = [
        'city'  =>  'require|max:100|unique:article',
        'keywords'  =>  'require',
    ];

    protected $message  =   [
        'name.require' => '姓名不能为空！',  
        'name.unique' => '姓名不能重复！',  
        'name.max' => '姓名不能大于100位！', 
        'keywords.require' => '关键字不能为空！', 

    ];



}