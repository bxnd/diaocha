ThinkPHP 5.0  问卷调查项目

运行环境：ThinkPHP5.0框架 

ThinkPHP5的环境要求如下：

- PHP >= 5.4.0
- PDO PHP Extension
- MBstring PHP Extension
- CURL PHP Extension

===============
 
 测试账号: admin 密码: admin
 
 后台登录 admin/login/index

 ## 问卷管理界面
![问卷管理界面](https://images.gitee.com/uploads/images/2020/0412/183504_d06e9fc1_1022917.png "show.png")


## 问卷统计界面 
![问卷统计](https://images.gitee.com/uploads/images/2021/0420/141427_eec64360_1022917.png "count.png")

 


## 部署
下载此文件调查zip源码后，解压的www目录。1.在database.php文件修改上正确的mysql连接地址账号和密码。2.把根目录的diaocha.sql文件导入到数据库。3.访问管理员后台www.domain.com/index.php/admin/login/index

## 使用
后台把 生成的问卷地址或者二维码 发给用户 开展调查

## 发现演示站被挂木马，会随机跳转到恶意网站，漏洞位置不详

## 部署问题更新：
1.导入直接连接的vendor目录的地址，需要直接开放根目录，并且项目名是固定的“gmt”,这个调用一直需要更为直接在app目录读取，3年前的问题了。放下后就不没有解决。
解决尝试：发现后期增加的验证码composer调用类访问为 Vendor("yanzheng"),phpexcel可以试试这个相同方式
2.问卷填写完成后 提示 缺失 memberid字段，没有完善
3.指定用户时，发现没有字段没有设置默认值问题。改进建议，把字段设置为必填，并设置默认值
